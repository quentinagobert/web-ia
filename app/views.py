from django.shortcuts import render
from random import randint
from .monia import algo

def home(request):
    context = {}
    return render(request, "index.html", context)


def guess(request):
    raw_data = request.POST.get("canvas")
    data_as_array = [int(x) for x in raw_data.split(",")[3::4]]
    guess = algo(data_as_array) # Là où ils appellent leur fonction d'IA
    return render(request, "partials/guess.html", { "guess": guess })